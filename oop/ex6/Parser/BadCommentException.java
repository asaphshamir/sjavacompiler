package oop.ex6.Parser;

public class BadCommentException extends CompilationException {
	public BadCommentException(){
		super("commment cannot start with whitespace\n");
	}
}
