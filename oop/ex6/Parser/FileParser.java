package oop.ex6.Parser;


import oop.ex6.SymbolTable.SymbolTable;
import oop.ex6.SymbolTable.VariableNotDeclaredException;
import oop.ex6.Variable.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileParser {

	private static final int GLOBAL_SCOPE = 0;

	private List<String> lines;
	private boolean isDec;
	private SymbolTable currentSymTable;

	private enum commandType{
		VAR_DEC, METHOD_DEC, ASSIGNMENT, IF, WHILE;
	}

	public FileParser(String filename) throws IOException{
		Path filepath = Paths.get(filename);
		lines = Files.readAllLines(filepath);
	}

	public List<String> getAllLines(){
		return lines;
	}


	private String getFirstWord(String line){
		return line.split("\\s")[0];
	}

	private void removeCommentsAndEmptyLines() throws CompilationException {
		for (Iterator<String> iterator = lines.iterator(); iterator.hasNext();) {
			String line = iterator.next();
			if (line.isEmpty() || checkComment(line)){
				iterator.remove();
			}
		}
	}

	private boolean checkComment(String line) throws CompilationException {
		Pattern commentPattern = Pattern.compile("(\\s*)//.*");
		Matcher matcher = commentPattern.matcher(line);
		if (matcher.matches()){
			if (matcher.group(0).isEmpty()){
				return true;
			}
			else {
				throw new BadCommentException();
			}
		}
		return false;
	}

	private void removeLeadingSpace(){
		for (String line : lines) {
			line = line.trim();
		}
	}

	private commandType getCommandType(String line){
		String firstWord = getFirstWord(line);
		if (firstWord.matches("int|double|boolean|String|char|final")){
			return commandType.VAR_DEC;
		}
		else if (firstWord.matches("void")){
			return commandType.METHOD_DEC;
		}
		else if (firstWord.matches("if")){
			return commandType.IF;
		}
		else if (firstWord.matches("while")){
			return commandType.WHILE;
		}
		else {
			return commandType.ASSIGNMENT;
		}
	}

	private void insertGlobals() throws CompilationException {
		int scope = GLOBAL_SCOPE;
		for (String line : lines) {
			if (scope == GLOBAL_SCOPE) {
				if (getCommandType(line) == commandType.VAR_DEC){
					isDec = true;
					parseVariablesDec(line);
				}
			}
			if (line.endsWith("{")) {
				scope++;
			}
			else if (line.equals("}")){
				scope--;
			}
		}
	}

	private void parseVariablesDec(String line) throws CompilationException {
		Pattern p = Pattern.compile("(final\\s++)?(.*)\\s++(.*)\\s*;");
		Matcher m = p.matcher(line);
		if (m.matches()){
			parseVariables(!(m.group(0).isEmpty()), m.group(1), m.group(2));
		}
		else {
			throw new BadVariableDeclaration();
		}
	}

	private void parseVariables(boolean isFinal, String varType, String varsString) throws CompilationException {
		String[] vars = varsString.split(",");
		for (String var : vars) {
			parseVariableDec(isFinal, varType, var);
		}
	}

	private void parseVariableDec(boolean isFinal, String varType, String var) throws CompilationException {
		Pattern p = Pattern.compile("((?:_\\w+)|(?:[a-zA-Z]\\w*))\\s*(?:(=)\\s*(.*))?");
		Matcher m = p.matcher(var);
		if (m.matches()) {
			if (isDec) {
				currentSymTable.insert(m.group(0), VariableFactory.getVariable(m.group(0), varType, isFinal,
						false));
				if (!(m.group(1) == null)) {
					checkAssignment(m.group(0), m.group(2));
				}
		}
		else {
			throw new BadVariableDeclaration();
		}
	}

	private void checkAssignment(String lhs, String rhs) throws CompilationException {
		Variable leftVar = currentSymTable.getVariable(lhs);
		Variable rightVar;
		try {
			rightVar = currentSymTable.getVariable(rhs);
		}
		catch (VariableNotDeclaredException e){
			rightVar = VariableFactory.getVariable(rhs, leftVar.getType(), false, true);
		}
		if (!(leftVar.getType().equals(rightVar.getType()))){
			throw new NotOfTypeException(rhs, leftVar.getType());
		}
		if (!isDec && leftVar.isFinal()){
			throw new FinalAssignmentException(lhs);
		}
	}



}
