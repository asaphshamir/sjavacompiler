package oop.ex6.SymbolTable;

import oop.ex6.Parser.CompilationException;

public class VariableNotDeclaredException extends CompilationException {
	public VariableNotDeclaredException(){
		super("variable was not declared in this scope\n");
	}
}
