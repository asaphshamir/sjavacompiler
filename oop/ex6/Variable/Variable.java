package oop.ex6.Variable;



public class Variable {

	private String varName;
	private String type;
	private boolean isFinal;

	private boolean initialized;

	public Variable(String varName, String type, boolean isFinal, boolean initialized){
		this.varName = varName;
		this.type = type;
		this.isFinal = isFinal;
		this.initialized = initialized;
	}

	public String getType(){
		return type;
	}

	public boolean isFinal() {
		return isFinal;
	}

	public boolean isInitialized() {
		return initialized;
	}

}
