package oop.ex6.Variable;

import oop.ex6.Parser.CompilationException;

public class NotOfTypeException extends CompilationException {
	public NotOfTypeException(String expression, String Type){
		super(expression + " is not of type " + Type);
	}
}
