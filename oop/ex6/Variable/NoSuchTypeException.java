package oop.ex6.Variable;

import oop.ex6.Parser.CompilationException;

public class NoSuchTypeException extends CompilationException {
	public NoSuchTypeException(String type){
		super("unknown type: " + type);
	}
}
