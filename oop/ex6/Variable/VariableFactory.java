package oop.ex6.Variable;

import oop.ex6.Parser.CompilationException;

public class VariableFactory {

	private static final boolean INITIALIZED = true;

	public static Variable getVariable(String expression, String type, boolean isFinal, boolean isConst)
			throws CompilationException {
		boolean typeMatch = false;
		if (isConst){
			switch (type){
				case "int":
					if (expression.matches("-?\\d+")){
						return new Variable(expression, "int", isFinal, INITIALIZED);
					}
					throw new NotOfTypeException(expression, type);

				case "double":
					if (expression.matches("-?\\d+(?:\\.\\d+)?")){
						return new Variable(expression, "double", isFinal, INITIALIZED);
					}
					throw new NotOfTypeException(expression, type);

				case "char":
					if (expression.matches("[^',\"\\\\]")){
						return new Variable(expression, "char", isFinal, INITIALIZED);
					}
					throw new NotOfTypeException(expression, type);

				case "String":
					if (expression.matches("[^',\"\\\\]*")){
						return new Variable(expression, "String", isFinal, INITIALIZED);
					}
					throw new NotOfTypeException(expression, type);

				case "boolean":
					if (expression.matches("true|false|-?\\d+(?:\\.\\d+)?")){
						return new Variable(expression, "boolean", isFinal, INITIALIZED);
					}
					throw new NotOfTypeException(expression, type);

				default:
					throw new NoSuchTypeException(type);
			}
		}
		else {
			return new Variable(expression, type, isFinal, !INITIALIZED);
		}
	}

}
